dbd(A, 0, A).
dbd(0, A, A).
dbd(A, B, D) :-
    (A>B),
    (B>0),
    R is A mod B,
    dbd(B,R,D).
dbd(A, B, D) :-
    (A<B),
    dbd(B,A,D).