findsecond([], Result).
findsecond(List, Result) :-
    getResult(List, 0, Result).

getResult([Head|Tail], Count, Result) :- 
    Count1 is Count+1,
    Count1 < 2,
    getResult(Tail, Count1, Result).
getResult([Head|Tail], Count, Head).