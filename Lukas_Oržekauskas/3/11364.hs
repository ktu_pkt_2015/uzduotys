﻿import Data.Char
import Data.List
import System.IO
import Control.Monad

calcMinDistance 0 sum i sNumStatic = return ()

calcMinDistance n sum i sNumStatic = do
    let j = sum
    let sum = j + (i!!(n-1)-i!!(n-2))
    if n == 2
        then do
            let j = sum
            let sum = j + (i!!(sNumStatic-1)-i!!0)
            putStrLn "Minimal distance for the current case:"
            print sum
        else do calcMinDistance (n-1) sum i sNumStatic

getStorePositions 0 a sNum = return ()

getStorePositions n a sNum = do
    sPos <- getLine
    let pos = read sPos :: Int
    let i = a
    let a = i ++ [pos]
    putStrLn "Position saved"
    putStrLn "Current store positions list for the current case:"
    print a
    if n == 1
        then do
            print "All store positions entered, calculating minimal distance for the current case"
            let i = sort a
            let sum = 0
            let sNumStatic = sNum
            calcMinDistance sNum sum i sNumStatic
        else do getStorePositions (n-1) a sNum

casesInfoInputsTimes 0 = return ()

casesInfoInputsTimes n = do
    putStrLn "Enter the number of stores to visit (1<=n<=20):"
    sNumber <- getLine
    let y = read sNumber :: Int
    let sNum = y
    putStrLn "Enter integer positions (0<=xi<=99) for each store one by one by pressing enter"
    let a = []
    getStorePositions y a sNum
    casesInfoInputsTimes (n-1)

main = do
    putStrLn "Enter number of test cases (1<=t<=100)"
    tCases <- getLine
    let x = read tCases :: Int
    casesInfoInputsTimes x