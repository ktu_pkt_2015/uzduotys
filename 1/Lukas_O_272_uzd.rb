#!/usr/bin/env ruby
class TEX
  def paleisti
    puts "Enter text with TEX quotes"
    a = gets.chomp
    pasibaigia = a.end_with?('"')
    pirmosarantros = true
    parts = a.split('"')
    #parts[0].insert(0, "` `")
    for i in 1..parts.count-1
      if i%2 == 0
        parts[i].insert(0, "' '")
        pirmosarantros = true
      else
        parts[i].insert(0, "` `")
        pirmosarantros = false
      end    
    end  
    
    if pasibaigia == true
      if pirmosarantros == true
        puts parts.join("") + "` `"
      else
        puts parts.join("") + "' '"
      end
    else
      puts parts.join("")
    end
  end
end

object = TEX. new
object.paleisti